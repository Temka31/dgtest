// ROOT REDUCER EXAMPLE

// 1. Import your reducers from entities

// import cartReducer from './entities/cart/reducers';

import { combineReducers } from 'redux';


// 2. Define reducers into common object
const rootReducer = combineReducers({
  // cart: cartReducer
});


export default rootReducer;
export type IRootReducer = ReturnType<typeof rootReducer>;