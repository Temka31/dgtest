import type { Context } from 'next-redux-wrapper';
import { createWrapper, HYDRATE } from 'next-redux-wrapper';
import type { AnyAction, Reducer } from 'redux';
import { applyMiddleware, createStore } from 'redux';
import thunkMiddleware from 'redux-thunk';

import type { IRootReducer } from './rootReducer';
import rootReducer from './rootReducer';

const bindMiddleware = (middleware: any) => {
  if (process.env.NODE_ENV !== 'production') {
    const { composeWithDevTools } = require('redux-devtools-extension');
    return composeWithDevTools(applyMiddleware(...middleware));
  }
  return applyMiddleware(...middleware);
};

const reducer: Reducer<IRootReducer, AnyAction> = (state, action) => {
  if (action.type === HYDRATE) {
    return { ...state, ...action.payload };
  } else {
    return rootReducer(state, action);
  }
};

const initStore = (_context: Context) => {
  return createStore(reducer, bindMiddleware([thunkMiddleware]));
};

export const wrapper = createWrapper(initStore);
