import axios from 'axios';
import getConfig from 'next/config';

export const {
  publicRuntimeConfig: { baseUrl }
} = getConfig();


const axiosInstance = axios.create({
  baseURL: baseUrl
});

export const objectToJson = (obj: Record<string, unknown>): string => {
  return JSON.stringify(obj, null, 0);
};

axiosInstance.interceptors.request.use(request => {
  // Type request middleware here
  return request;
}, error => {
  return Promise.reject(error);
});

axiosInstance.interceptors.response.use(response => {
  // Type response middleware here
  return response;
}, error => {
  return Promise.reject(error);
});

export default axiosInstance;
