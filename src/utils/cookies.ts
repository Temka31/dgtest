// Use this instance for working with cookies in runtime
import Cookies from "universal-cookie";

export const _COOKIES = new Cookies();