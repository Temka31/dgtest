import React, { ComponentType, useEffect } from 'react';
import debounce from 'lodash.debounce';

const withVhFix = (WrappedComponent: ComponentType<any>) => {
  const WithVhFix = (props: any) => {
    const vhfix = debounce(() => {
      const vh = window.innerHeight * 0.01;
      document.documentElement.style.setProperty('--vh', `${vh}px`);
    }, 100);

    useEffect(() => {
      vhfix();
      window.addEventListener('resize', vhfix);

      return () => {
        window.removeEventListener('resize', vhfix);
      };
      // eslint-disable-next-line
    }, []);

    return <WrappedComponent {...props} />;
  };

  return WithVhFix;
};

export default withVhFix;
