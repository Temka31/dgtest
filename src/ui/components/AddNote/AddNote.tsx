import React from 'react';
import Select from 'react-select';
import classNames from 'classnames';

// import { TCol, TRow } from '@/ui/components/Table/Table';
import styles from './AddNote.module.scss';

type TProps = {
  newNote: {
    name: string;
    surName: string;
    age: string;
    city: { value: string; label: string };
  };
  setNewNote: (value: any) => void;
  addNewNote: () => void;
};
const options = [
  { value: 'Riga', label: 'Riga' },
  { value: 'Daugavpils', label: 'Daugavpils' },
  { value: 'Jūrmala', label: 'Jūrmala' },
  { value: 'Ventspils', label: 'Ventspils' },
];

const AddNote = ({ newNote, setNewNote, addNewNote }: TProps): JSX.Element => {
  const disable = !newNote.age || !newNote.surName || !newNote.name || !newNote.city.value;
  const renderInput = () => {
    const keys = Object.keys(newNote);
    return keys.map((item) => {
      if (item == 'city') return;
      return (
        <input
          className={styles.input}
          // @ts-ignore
          value={newNote[item]}
          placeholder={item}
          onChange={(e) =>
            setNewNote((prevState: { [key: string]: string }) => ({
              ...prevState,
              [item]: e.target.value,
            }))
          }
        />
      );
    });
  };

  return (
    <div className={styles.main}>
      {renderInput()}
      <Select
        isMulti={false}
        className={styles.select}
        // @ts-ignore
        options={options}
        value={newNote.city}
        onChange={(value) =>
          setNewNote((prevState: { [key: string]: string }) => ({ ...prevState, city: value }))
        }
      />
      <button
        disabled={disable}
        className={classNames(styles.btn, { [styles.disable]: disable })}
        onClick={addNewNote}
      >
        ADD
      </button>
    </div>
  );
};

export default AddNote;
