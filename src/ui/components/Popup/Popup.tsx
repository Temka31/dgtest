import React, { FC, useEffect, useRef, useState } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import usePortal from 'react-useportal';

import { TPopupProps } from './Popup.d';
import PopupModal from './PopupModal';

const NULL_EVENT = { currentTarget: { contains: () => false } };

const Popup: FC<TPopupProps> = (props) => {
  const {
    mix = '',
    shown = false,
    centered = true,
    noPadding = false,
    closeButtonOut = false,
    closeButtonBackground = false,
    children,
    onClose,
    onOpen,
    onEnter,
    closeButton = false,
    preventClose,
    onPreventClose,
    changePosition,
    noOverflow = false,
    closeOld = false,
  } = props;
  const modalRef = useRef<HTMLDivElement>(null);
  const [isShown, setShown] = useState(false);
  const [animateClass, setAnimateClass] = useState('');
  const { openPortal, closePortal, isOpen, Portal } = usePortal({
    closeOnOutsideClick: false,
    closeOnEsc: true,
    isOpen: isShown,
    bindTo:
      ((typeof document !== 'undefined' && document.querySelector('.js-portal')) as HTMLElement) ||
      ((typeof document !== 'undefined' && document.getElementById('__next')) as HTMLElement) ||
      ((typeof document !== 'undefined' && document.querySelector('body')) as HTMLElement),
  });

  const handleClose = (): void => {
    console.log('clouse');
    setAnimateClass('fadeOut');

    if (modalRef.current) {
      modalRef.current.addEventListener('animationend', () => {
        closePortal();
        setShown(false);
        if (onClose) onClose();
      });
    }
  };

  const handleOpen = (refBlock?: any): void => {
    openPortal(NULL_EVENT);
    setShown(true);
    if (onOpen) onOpen(refBlock);
  };

  useEffect(() => {
    if (!shown) {
      handleClose();
    } else if (shown) {
      handleOpen();
    }
  }, [shown]);

  return isOpen ? (
    <Portal>
      <PopupModal
        modalRef={modalRef}
        setAnimateClass={setAnimateClass}
        animateClass={animateClass}
        centered={centered}
        noPadding={noPadding}
        noOverflow={noOverflow}
        closeButton={closeButton}
        mix={mix}
        onEnter={onEnter}
        onClose={(): void => {
          if (preventClose) {
            if (onPreventClose) onPreventClose();
          } else {
            handleClose();
          }
        }}
        onOpen={(ref): void => handleOpen(ref)}
        closeButtonOut={closeButtonOut}
        closeButtonBackground={closeButtonBackground}
        changePosition={changePosition}
        closeOld={closeOld}
      >
        {children}
      </PopupModal>
    </Portal>
  ) : null;
};

export default Popup;
