import React, { useEffect } from 'react';
// eslint-disable-next-line import/no-extraneous-dependencies
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import classNames from 'classnames';

import { TPopupModalProps } from './Popup.d';

import styles from './Popup.module.scss';

const PopupModal = ({
  children,
  mix,
  onClose,
  onOpen,
  onEnter,
  centered,
  modalRef,
  animateClass,
  setAnimateClass,
  noPadding = false,
  noOverflow = false,
}: TPopupModalProps): JSX.Element => {
  const handleKeyDown = (e: KeyboardEvent): void => {
    if (e.keyCode === 27) onClose();
  };

  const handleClick = (e: MouseEvent): void => {
    const target = e.target as HTMLElement;

    if (!target.closest('.js-trigger') && !target.closest('.js-modal') && target.closest('body')) {
      onClose();
    }
  };

  useEffect(() => {
    if (modalRef.current) {
      disableBodyScroll(modalRef.current, {
        reserveScrollBarGap: true,
        allowTouchMove: (el: any) => {
          while (el && el !== document.body) {
            if (el.getAttribute('body-scroll-lock-ignore') !== null) {
              return true;
            }

            if (el.parentElement) el = el.parentElement;
          }
          return false;
        },
      });
      setAnimateClass('fadeIn');

      if (onOpen) onOpen(modalRef);
    }

    document.addEventListener('mousedown', handleClick);
    document.addEventListener('keydown', handleKeyDown);

    return (): void => {
      // if (modalRef.current) {
      enableBodyScroll(document.body);
      // }
      document.removeEventListener('mousedown', handleClick);
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  return (
    <div
      className={classNames(
        styles.wrapper,
        animateClass,
        {
          [styles.notCentered]: !centered,
          [styles.noPadding]: noPadding,
          [styles.noOverflow]: noOverflow,
        },
        'js-modal-wrapper'
      )}
      ref={modalRef}
      onKeyDown={(e) => {
        if (e.keyCode == 13 && onEnter) {
          e.preventDefault();
          onEnter();
        }
      }}
    >
      <div className={classNames(styles.inner, 'js-modal', { [styles.noPadding]: noPadding }, mix)}>
        <div className={classNames(styles.content, 'js-content')}>{children}</div>
      </div>
    </div>
  );
};

export default PopupModal;
