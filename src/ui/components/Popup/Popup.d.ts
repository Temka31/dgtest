import { SetStateAction } from 'react';

export type TPopupProps = {
  noPadding?: boolean;
  noOverflow?: boolean;
  centered?: boolean;
  closeButton?: boolean;
  closeButtonBackground?: boolean;
  preventClose?: boolean;
  onPreventClose?: () => void;
  onEnter?: () => void;
  /**
   * Кнопка закрытия снаружи модального окна
   */
  closeButtonOut?: boolean;
  /**
   * Кнопка для открытия
   */
  trigger?: React.ReactNode;
  /**
   * Контент внутри попапа
   */
  children: React.ReactChildren | React.ReactChild;
  /**
   * показывать/не показывать попап
   */
  shown?: boolean;
  /**
   * 	Функция, которая вызывается при закрытии попапа.
   */
  onClose?: () => void;
  /**
   * 	Функция, которая вызывается при открытии попапа.
   */
  onOpen?: (ref: RefObject<HTMLDivElement>) => void;
  /**
   * Дополнительные классы, не привязанные к стилям
   */
  mix?: string;
  changePosition?: string;
  closeOld?:boolean
};

export type TPopupModalProps = {
  noPadding?: boolean;
  noOverflow?: boolean;
  centered?: boolean;
  closeButton?: boolean;
  closeButtonBackground?: boolean;
  modalRef: RefObject<HTMLDivElement>;
  animateClass: string;
  setAnimateClass: Dispatch<SetStateAction<string>>;
  /**
   * Контент внутри попапа
   */
  children: React.ReactNode;
  /**
   * Кастомный класс
   */
  mix?: string;
  /**
   * Кнопка закрытия снаружи модального окна
   */
  closeButtonOut?: boolean;
  /**
   * 	Функция, которая вызывается при закрытии попапа.
   */
  onEnter?: () => void;
  onClose: () => void;
  /**
   * 	Функция, которая вызывается при открытии попапа.
   */
  onOpen?: (ref: RefObject<HTMLDivElement>) => void;
  changePosition?: string;
  closeOld?:boolean
};
