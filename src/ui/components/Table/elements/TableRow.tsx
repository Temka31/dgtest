import React from 'react';

import styles from '../Table.module.scss';
import classNames from 'classnames';

import { TRowItem } from '../Table';
type TProps = {
  items: TRowItem[];
};

const TableRow = ({ items }: TProps): JSX.Element => (
  <tr className={classNames(styles.row)}>
    {items.map(({ content }, i) => (
      <td key={i} className={styles.cell}>
        {content}
      </td>
    ))}
  </tr>
);

export default TableRow;
