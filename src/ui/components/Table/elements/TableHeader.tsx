import React from 'react';

import styles from '../Table.module.scss';

import { TCol } from '../Table';
type TProps = {
  items: TCol[];
};

const TableHeader = ({ items }: TProps): JSX.Element => (
  <tr className={styles.header}>
    {items.map(({ name }, i) => (
      <th key={`${name}-${i}`} className={styles.headerItem}>
        {name}
      </th>
    ))}
  </tr>
);

export default TableHeader;
