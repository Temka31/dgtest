import React from 'react';
import classNames from 'classnames';

import TableHeader from './elements/TableHeader';
import TableRow from './elements/TableRow';

import styles from './Table.module.scss';

export type TRowItem = { content: string | JSX.Element | null };
export type TCol = { name: string };
export type TRow = { items: TRowItem[] };
type TProps = {
  cols: TCol[];
  rows: TRow[];
  mix?: string;
};

const Table = ({ cols, rows, mix }: TProps): JSX.Element => {
  return (
    <table className={classNames(styles.root, mix)}>
      <thead>
        <TableHeader items={cols} />
      </thead>
      <tbody>
        {rows.map(({ items }, i) => (
          <TableRow key={i} items={items} />
        ))}
      </tbody>
    </table>
  );
};

export default Table;
