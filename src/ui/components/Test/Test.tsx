import { useState } from 'react';
import Icon from 'src/assets/icons/x.svg';

import AddNote from '@/ui/components/AddNote/AddNote';
import Popup from '@/ui/components/Popup/Popup';
import Table from '@/ui/components/Table/Table';

import styles from './Test.module.scss';

const HISTORY_COLS = [
  { name: 'Name' },
  { name: 'Surname' },
  { name: 'Age' },
  { name: 'City' },
  { name: 'Actions' },
];
type TRows = {
  name: string;
  surName: string;
  age: string;
  city: { value: string; label: string };
};

const initNewItem = { name: '', surName: '', age: '', city: { value: '', label: '' } };
const Test = () => {
  const [newItem, setNewItem] = useState(initNewItem);
  const [tables, setTables] = useState([{ items: [initNewItem] }]);
  const [popup, setPopup] = useState(false);
  const [editRow, setEditRow] = useState(initNewItem);
  const [row, setRow] = useState({ table: 0, index: 0 });
  const onClickPopup = () => setPopup(!popup);

  const addRow = () => {
    const newTables = [...tables];
    newTables[0].items.push(newItem);
    setTables(newTables);
    setNewItem(initNewItem);
  };

  const delTable = (index: number) => {
    if (!index) {
      alert('Нельзя удалить основную таблицу');
      return;
    }
    const newTables = [...tables];
    newTables.splice(index, 1);
    setTables(newTables);
  };

  const delRow = (table: number, index: number) => {
    const newTable = JSON.parse(JSON.stringify(tables));
    newTable[table].items.splice(index, 1);
    setTables(newTable);
  };

  const copyTable = (index: number) => {
    const newTable = JSON.parse(JSON.stringify(tables));
    newTable.splice(index + 1, 0, { ...tables[index] });
    setTables(newTable);
  };

  const handleEditRow = (table: number, index: number) => {
    setPopup(true);
    setRow({ table, index });
    setEditRow(tables[table].items[index]);
  };

  const updateRow = () => {
    const newTables = JSON.parse(JSON.stringify(tables));
    newTables[row.table].items[row.index] = editRow;
    setTables(newTables);
    setPopup(false);
  };

  const mapRows = (items: TRows[], table: number) =>
    items.map((item, index) => {
      if (!item.name) return { items: [] };
      return {
        items: [
          { content: item.name },
          { content: item.surName },
          { content: item.age },
          { content: item.city.label },
          {
            content: (
              <div>
                <button className={styles.button} onClick={() => handleEditRow(table, index)}>
                  Edit
                </button>
                <button className={styles.button} onClick={() => delRow(table, index)}>
                  Delete
                </button>
              </div>
            ),
          },
        ],
      };
    });
  return (
    <div className="container">
      <AddNote newNote={newItem} setNewNote={setNewItem} addNewNote={addRow} />

      {tables.map((item, index) => (
        <div>
          <div className={styles.buttons}>
            <button className={styles.clone} onClick={() => copyTable(index)}>
              Copy Table
            </button>
            <button onClick={() => delTable(index)}>
              <Icon width="14px" height="14px" />
            </button>
          </div>
          <Table cols={HISTORY_COLS} rows={mapRows(item.items, index)} />
        </div>
      ))}

      <Popup
        shown={popup}
        preventClose={true}
        closeButton={true}
        centered={true}
        onPreventClose={onClickPopup}
        noPadding={true}
        // mix={styles.allWidth}
      >
        <div>
          <AddNote newNote={editRow} setNewNote={setEditRow} addNewNote={updateRow} />
        </div>
      </Popup>
    </div>
  );
};

export default Test;
