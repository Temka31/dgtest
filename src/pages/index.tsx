import type { IMetaData } from '@/types';

import type { GetServerSideProps, NextPage } from 'next';
import dynamic from 'next/dynamic';

import Test from '@/ui/components/Test/Test';
const Layout = dynamic(() => import('@/ui/components/Layout/Layout'));

interface IndexPageProps {
  meta?: IMetaData;
}
const IndexPage: NextPage<IndexPageProps> = ({ meta }) => {
  return (
    <Layout meta={meta}>
      <Test />
    </Layout>
  );
};

export const getServerSideProps: GetServerSideProps = async () => {
  const meta = {
    title: 'Главная страница',
    description: 'Описание главной страницы',
    keywords: 'ключевые, слова, главной, страницы',
  };

  return {
    props: {
      meta,
    },
  };
};

export default IndexPage;
