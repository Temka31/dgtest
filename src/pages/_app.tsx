import type { AppProps } from 'next/app';

import '@/assets/styles/index.scss';

import { wrapper } from '@/store';

function AppWithRedux({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />;
}

export default wrapper.withRedux(AppWithRedux);
